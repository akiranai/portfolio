var data = [
    {
      LargerScreen: "<span><b>root</b>@localhost: whoami<br/><br/># First name: Anthony<br/># Last name: Tayar <br/># Age: 24<br/># Location: Belgium<br/># Hobbies: <br/>--- Video games;<br/>--- FOSS;<br/>--- Travelling and trekking;<br/>--- Testing too much different Linux distro and broke them.</span><br/><br/><span># Contact : <br/> --- +32494637770<br/> --- anthony.tayar@protonmail.com</span><br> ",
      AboutProject:"<span><b>root</b>@localhost: alias cat=please show<br/><span><b>root</b>@localhost: please show projects ",
      frIndex: "<span><b>root</b>@localhost: whoami<br/><br/># Prénom: Anthony<br/># Nom: Tayar <br/># Âge: 24<br/># Pays: Belgium<br/># Hobbies: <br/>--- Jeux vidéos;<br/>--- FOSS;<br/>--- Voyager et le trekking;<br/>--- Tester des distributions linux et les casser.</span><br/><br/><span># Contact : <br/> --- +32494637770<br/> --- anthony.tayar@protonmail.com</span><br>",
      frProject: "<span><b>root</b>@localhost: alias cat=svp montrez<br/><span><b>root</b>@localhost: svp montrez projets "
    }
  ];
  
  var allElements = document.getElementsByClassName("typeing");
  for (var j = 0; j < allElements.length; j++) {
    var currentElementId = allElements[j].id;
    var currentElementIdContent = data[0][currentElementId];
    var element = document.getElementById(currentElementId);
    var devTypeText = currentElementIdContent;
  
    // type code
    var i = 0, isTag, text;
    (function type() {
      text = devTypeText.slice(0, ++i);
      if (text === devTypeText) return;
      element.innerHTML = text + `<span class='blinker'>&#32;</span>`;
      var char = text.slice(-1);
      if (char === "<") isTag = true;
      if (char === ">") isTag = false;
      if (isTag) return type();
      setTimeout(type, 16);
    })();
  }
  